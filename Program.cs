﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("(Примечание: во всех задачах возможен ввод дробных чисел (где это возможно), ввод дробной части через запятую.) ");
            Console.WriteLine();

            TwoNumbersSum();
            RectangleAreaCalculating();
            CircleAreaCalculating();
            ParityNumberDetection();
            ArrayElementsSumCalculating();

            Console.ReadKey();

            void TwoNumbersSum()
            {
                /* Задача №1 - Расчет суммы двух чисел */
                float number1;
                float number2;
                float sum;

                Console.WriteLine("ЗАДАЧА №1 - Расчет суммы двух чисел. ");
                Console.WriteLine("Введите первое число: ");
                number1 = (float)Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите второе число: ");
                number2 = (float)Convert.ToDouble(Console.ReadLine());

                sum = number1 + number2;

                Console.WriteLine($"Сумма чисел равна {sum}.");
                Console.WriteLine();
                /* Конец задачи №1 */
            }

            void RectangleAreaCalculating()
            {
                /* Задача №2 - Расчет площади прямоугольника по введенным значения длины и ширины*/
                float length;
                float width;
                float rectangleArea;

                Console.WriteLine("ЗАДАЧА №2 - Расчет площади прямоугольника по введенным значения длины и ширины. ");
                Console.WriteLine("Введите значение длины: ");
                length = (float)Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите значение ширины: ");
                width = (float)Convert.ToDouble(Console.ReadLine());

                rectangleArea = length * width;

                Console.WriteLine($"Площадь прямоугольника равна {rectangleArea}.");
                Console.WriteLine();
                /* Конец задачи №2 */
            }

            void CircleAreaCalculating()
            {
                /* Задача №3 - Расчет площади круга. Радиус вводится с клавиатуры */
                float radius;
                float pi = 3.14f;
                float circleArea;

                Console.WriteLine("ЗАДАЧА №3 - Расчет площади круга. Радиус вводится с клавиатуры. ");
                Console.WriteLine("Введите значение радиуса: ");
                radius = (float)Convert.ToDouble(Console.ReadLine());

                circleArea = pi * radius * radius;

                Console.WriteLine($"Площадь круга равна {circleArea}.");
                Console.WriteLine();
                /* Конец задачи №3 */
            }

            void ParityNumberDetection()
            {
                /* Задача №4 - Определение четности/нечетности введенного числа */
                int number;

                Console.WriteLine("ЗАДАЧА №4 - Определение четности/нечетности введенного числа. ");
                Console.WriteLine("Введите число: ");
                number = Convert.ToInt32(Console.ReadLine());

                if ((number % 2) != 0)
                {
                    Console.WriteLine($"Число {number} является нечетным. ");
                }
                else
                {
                    Console.WriteLine($"Число {number} является четным. ");
                }
                Console.WriteLine();
                /* Конец задачи №4 */
            }

            void ArrayElementsSumCalculating()
            {
                /* Задача №5 - Расчет суммы элементов массива. */
                float[] array = { };
                int arraySize;
                float arraySum = 0f;

                Console.WriteLine("ЗАДАЧА №5 - Расчет суммы элементов массива. ");
                Console.WriteLine("Введите число элементов массива: ");
                arraySize = Convert.ToInt32(Console.ReadLine());
                array = new float[arraySize];

                for (int i = 0; i < arraySize; i++)
                {
                    Console.WriteLine($"Введите {i + 1}-й элемент массива: ");
                    array[i] = Convert.ToInt32(Console.ReadLine());
                    arraySum += array[i];
                }

                Console.WriteLine($"Сумма всех элементов массива равна {arraySum}. ");
                Console.WriteLine();
                /* Конец задачи №5 */
            }
        }
    }
}
